package com.example.demo.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.module.Actors;

@RestController
public class ActorsController {

	@GetMapping(value = "actors")

	public List<Actors> getActors() {
		List<Actors> li = new ArrayList<Actors>();
		li.add(new Actors("vijay", "rakshitha", "Ravishankar", "vinod", "ramnath"));
		return li;

	}
}
