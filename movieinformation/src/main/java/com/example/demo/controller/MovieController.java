package com.example.demo.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.example.demo.module.Actors;
import com.example.demo.module.MovieInformation;

@RestController
public class MovieController {
	@Autowired
	RestTemplate restTemplet;
	
   @GetMapping(value="movies/actors")
   
	public ResponseEntity<Actors[]> getAllmoviedetails() {
		List<MovieInformation> li = new ArrayList<MovieInformation>();
		li.add(new MovieInformation("123", "kannada movie", 4, "ram"));
		ResponseEntity<Actors[]> actors=restTemplet.getForEntity("http://localhost:8082/actors", Actors[].class);
		return actors;
		}
	

}
