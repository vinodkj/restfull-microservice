package com.example.demo.module;

public class MovieInformation {
	private String id;
	private String desc;
	private int ratings;
	private String name;

	

	public MovieInformation(String id, String desc, int ratings, String name) {
		super();
		this.id = id;
		this.desc = desc;
		this.ratings = ratings;
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public int getRatings() {
		return ratings;
	}

	public void setRatings(int ratings) {
		this.ratings = ratings;
	}

	public MovieInformation(String id, String desc, int ratings) {
		super();
		this.id = id;
		this.desc = desc;
		this.ratings = ratings;
	}

}
