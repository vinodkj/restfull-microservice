package com.example.demo.module;

import java.util.List;

public class ActorList {

	private List<Actors> actors;

	public List<Actors> getActors() {
		return actors;
	}

	public void setActors(List<Actors> actors) {
		this.actors = actors;
	}

}
