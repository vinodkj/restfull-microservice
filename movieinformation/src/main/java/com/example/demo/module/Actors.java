package com.example.demo.module;

public class Actors {
	private String heroName;
	private String heroinName;
	private String mainVilan;
	private String director;
	private String producer;
	
	
	public Actors() {
		
	}
	
	
	public Actors(String heroName, String heroinName, String mainVilan, String director, String producer) {
		super();
		this.heroName = heroName;
		this.heroinName = heroinName;
		this.mainVilan = mainVilan;
		this.director = director;
		this.producer = producer;
	}

	public String getHeroName() {
		return heroName;
	}

	public void setHeroName(String heroName) {
		this.heroName = heroName;
	}

	public String getHeroinName() {
		return heroinName;
	}

	public void setHeroinName(String heroinName) {
		this.heroinName = heroinName;
	}

	public String getMainVilan() {
		return mainVilan;
	}

	public void setMainVilan(String mainVilan) {
		this.mainVilan = mainVilan;
	}

	public String getDirector() {
		return director;
	}

	public void setDirector(String director) {
		this.director = director;
	}

	public String getProducer() {
		return producer;
	}

	public void setProducer(String producer) {
		this.producer = producer;
	}

	

}
